## FrozenLake Tweaks

```
root@muryoutaisuu-virtual-machine:/usr/local/lib/python3.6/dist-packages/gym/envs/toy_text# grep -Ri natii ../../
../../envs/__init__.py:    id='FrozenLakeNatii-v0',
../../envs/__init__.py:    entry_point='gym.envs.toy_text:FrozenLakeNatiiEnv',
../../envs/toy_text/__init__.py:from gym.envs.toy_text.frozen_lake_natii import FrozenLakeNatiiEnv
../../envs/toy_text/frozen_lake_natii.py:class FrozenLakeNatiiEnv(discrete.DiscreteEnv):
../../envs/toy_text/frozen_lake_natii.py:        super(FrozenLakeNatiiEnv, self).__init__(nS, nA, P, isd)
```

## Compilation

For correct compilation in Visual Studio Code, add following Options to your User Settings:

```
{
    "latex-workshop.message.update.show": false,
    "latex-workshop.latex.recipes": [
        {
            "name": "latexmk",
            "tools": [
                "latexmk"
            ]
        },
        {
            "name": "pdflatex -> bibtex -> pdflatex*2",
            "tools": [
                "pdflatex",
                "bibtex",
                "pdflatex",
                "pdflatex"
            ]
        },
        {
            "name": "pdflatex -> makeglossaries -> pdflatex*2",
            "tools": [
                "pdflatex",
                "makeglossaries",
                "pdflatex",
                "pdflatex"
            ]
        }
    ],
    "latex-workshop.latex.tools": [
        {
            "name": "latexmk",
            "command": "latexmk",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "-pdf",
                "%DOC%"
            ]
        },
        {
            "name": "pdflatex",
            "command": "pdflatex",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "%DOC%"
            ]
        },
        {
            "name": "bibtex",
            "command": "bibtex",
            "args": [
                "%DOCFILE%"
            ]
        },
        {
            "name": "makeglossaries",
            "command": "makeglossaries",
            "args": [
                "%DOCFILE%"
            ]
        }
    ]
}
```

Afterwards Compile using `SHIFT + P` -> Search for "LaTeX Workshop: Build with recipe" -> Select "pdflatex -> makeglossaries -> pdflatex*2"

All subsequent Builds with `ALT + B` will be built correctly
