# https://github.com/kvfrans/openai-cartpole/blob/master/cartpole-random.py

import gym
import numpy as np
import matplotlib.pyplot as plt

env = gym.make('CartPole-v0')

def run_episode(env, parameters):
    observation = env.reset()
    totalreward = 0
    print('new episode')
    for _ in range(200):
        action = 0 if np.matmul(parameters,observation) < 0 else 1
        observation, reward, done, _ = env.step(action)
        print(reward)
        totalreward += reward
        if done:
            break
    return totalreward

def train(submit):
    counter = 0
    bestparams = None
    bestreward = 0
    for _ in range(10000):
        counter += 1
        parameters = np.random.rand(4) * 2 - 1
        reward = run_episode(env,parameters)
        if reward > bestreward:
            bestreward = reward
            bestparams = parameters
            if reward == 200:
                break
    return counter


# create graphs
results = []
for _ in range(1000):
    results.append(train(submit=False))
    # print(results[-1])

print("Durchschnitt: "+str(np.sum(results) / 1000.0))
