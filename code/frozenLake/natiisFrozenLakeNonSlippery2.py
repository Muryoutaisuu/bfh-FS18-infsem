# https://medium.com/emergent-future/simple-reinforcement-learning-with-tensorflow-part-0-q-learning-with-tables-and-neural-networks-d195264329d0


import gym
import numpy as np
from time import sleep
from gym.envs.registration import register


register( id='noSlipFrozenLake-v0', entry_point='gym.envs.toy_text:FrozenLakeNatiiEnv', kwargs={'desc':None,'map_name':'4x4','is_slippery':False} )
env = gym.make('noSlipFrozenLake-v0')


#Initialize table with all zeros
Q = np.zeros([env.observation_space.n,env.action_space.n])
#Initialize table with all 0.1 values
#Q = np.empty([env.observation_space.n, env.action_space.n])
#Q.fill(0.1)
# Set learning parameters
lr = .8
y = .95
num_episodes = 1000
#create lists to contain total rewards and steps per episode
#jList = []
rList = []
for i in range(num_episodes):
    if i % 1 == 0:
        print(f"---------------------------  Episode {i}")
        #sleep(0.1)

    #Reset environment and get first new observation
    s = env.reset()
    rAll = 0
    d = False
    j = 0
    #The Q-Table learning algorithm
    while j < 99:
        j+=1
        #Choose an action by greedily (with noise) picking from Q table
        a = np.argmax(Q[s,:] + np.random.randn(1,env.action_space.n)*(1./(i+1)))
        #Get new state and reward from environment
        s1,r,d,_ = env.step(a)
        #Update Q-Table with new knowledge
        Q[s,a] = Q[s,a] + lr*(r + y*np.max(Q[s1,:]) - Q[s,a])
        rAll += r
        s = s1
        sleep(0.1)
        if d == True:
            if r == 1:
                print(f"------------------------------ Episode: {i}")
                env.render()
                #print(Q)
                qtransposed = Q.transpose()
                arrays = np.split(qtransposed, 4)
                print(f"up:\n{np.round(arrays[3].reshape(4,4),4)}")
                print(f"right:\n{np.round(arrays[2].reshape(4,4),4)}")
                print(f"down:\n{np.round(arrays[1].reshape(4,4),4)}")
                print(f"left:\n{np.round(arrays[0].reshape(4,4),4)}")
                #_ = input('press Enter to continue')
            break
    #jList.append(j)
    rList.append(rAll)


print("Score over time: " +  str(sum(rList)/num_episodes))

print("Final Q-Table Values")
print(Q)


# print(rList)
